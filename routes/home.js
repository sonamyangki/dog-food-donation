const express = require("express");
const router = express.Router();

router.get("/", (req,res) => {
	res.render("home/welcome");
});

router.get("/home/about-us", (req,res) => {
	res.render("home/aboutUs", { title: "About Us | Druk Blood" });
});

router.get("/home/mission", (req,res) => {
	res.render("home/mission", { title: "Home| Druk Blood" });
});

router.get("/home/contact-us", (req,res) => {
	res.render("home/contactUs", { title: "Contact us | Druk Blood" });
});


module.exports = router;