const express = require("express");
const router = express.Router();
const middleware = require("../middleware/index.js");


router.get("/admin/dashboard", middleware.ensureAdminLoggedIn, async (req,res) => {
	res.render("admin/dashboard", {
		title: "Dashboard",
		
	});
});


module.exports = router;