const express = require("express");
const router = express.Router();
const middleware = require("../middleware/index.js");
const User = require("../models/user.js");
const Donation = require("../models/donation.js");

router.get("/donor/dashboard", middleware.ensureDonorLoggedIn, (req, res) => {
  // Render only the donorSidebar view from the "views/partials" folder
  res.render("partials/donorSidebar", {
    title: "Donor Dashboard Sidebar"
  });
});

// router.get("/dashboard", middleware.ensureDonorLoggedIn,(req,res) => {
// 	res.render("partials/donorSidebar", { title: "Donor Dashboard Sidebar" });
// });

module.exports = router;
